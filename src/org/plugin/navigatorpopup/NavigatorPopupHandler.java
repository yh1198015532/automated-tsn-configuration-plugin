package org.plugin.navigatorpopup;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.plugin.tsnsched.views.ConfigurationView;
import org.plugin.tsnsched.views.EventView;
import org.plugin.tsnsched.views.ScheduleView;


// This handler will obtain the core information about a file from a navigator popup menu

public class NavigatorPopupHandler extends AbstractHandler {
    private IWorkbenchWindow window;
    private IWorkbenchPage activePage;

    private IProject theProject;
    private IResource theResource;
    private IFile theFile;

    private String workspaceName;
    private String projectName;
    private String fileName;
    private String path;
    
    public NavigatorPopupHandler() {
        // Empty constructor
    }

    public Object execute(ExecutionEvent event) throws ExecutionException {
        // Get the project and file name from the initiating event if at all possible
        if(!extractProjectAndFileFromInitiatingEvent(event)) {
            return null;
        }
        // React to the popup selected
    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();


        path = theFile.getLocation().toString();
        // Set the filepath in the plugin
        org.plugin.tsnsched.Activator.filepath = path;
        ParseVecFile();


        return null;
    }

    private void ParseVecFile() {
		//open the file
    	List<String>vectorIdList = new ArrayList<String>();
    	List<String>vectorDelayList = new ArrayList<String>();
    	
    	HashMap<String, String>idMap = new HashMap<>();
    	HashMap<String, String>delayMap = new HashMap<>();
    	try (BufferedReader br = new BufferedReader(new FileReader(path))) {
    	    String line;
    	  //get vector id for each statistic
    	    while ((line = br.readLine()) != null) {
    	       //
    	    	if(line.length()==0){continue;}
    	    	if(Character.isDigit(line.charAt(0))){
    	    		break; //reached the end of vectors
    	    	}
    	    	
    	    	// process the line.
    	    	if(line.contains("pktRcvdFlowId")){
    	    		String[] entry = line.split(" ");
    	    		//vector 165 scenario2.target_workstation1.trafGenSchedApp pktRcvdFlowId:vector ETV
    	    		if(entry[2].contains("trafGenSchedApp")){continue;} //Skip if it's trafgen
    	    		vectorIdList.add(entry[1]);
    	    	}
    	    	else if (line.contains("pktRcvdDelay")){
    	    		String[] entry = line.split(" ");
    	    		//vector 165 scenario2.target_workstation1.trafGenSchedApp pktRcvdFlowId:vector ETV
    	    		if(entry[2].contains("trafGenSchedApp")){continue;} //Skip if it's trafgen
    	    		vectorDelayList.add(entry[1]);
    	    	}
    	    }
    	    // done fetching vector ID, 
    	    //read vector lines
    	    Set<String> uniqueId = new HashSet<String>();
    	    do{
    	    	//Skip lines that are not in the lists
    	    	String[] entry = line.split("\\t");
    	    	if(vectorIdList.contains(entry[0])){
    	    		idMap.put(entry[1], entry[3]);
    	    		uniqueId.add(entry[3]);
    	    	}
    	    	else if(vectorDelayList.contains(entry[0])){
    	    		delayMap.put(entry[1], entry[3]);
    	    	}
    	    }
    	    while ((line = br.readLine()) != null);
    	    
    	    
    	    
    	    HashMap<String, List<String>>resultMap = new HashMap<String, List<String>>();
    	    HashMap<String,Double> minMap = new HashMap<>();
    	    HashMap<String,Double> maxMap= new HashMap<>();
    	    HashMap<String,Double> avgMap = new HashMap<>();
    	    
    	    for (String string : uniqueId) {
				resultMap.put(string, new ArrayList<>());
				minMap.put(string, Double.MAX_VALUE);
				maxMap.put(string, Double.MIN_NORMAL);
				avgMap.put(string, 0.0);
			}
    	    
        	//Save each flow as a Arraylist
        	for (Entry<String, String> entry : idMap.entrySet()) {
        	    String key = entry.getKey();
        	    String flow = entry.getValue();
        	    resultMap.get(flow).add(delayMap.get(key));
        	    double value = Double.parseDouble(delayMap.get(key));
        	    if(value<minMap.get(flow)){minMap.put(flow,value);} //Is this smaller then previous value?
        	    if(value>maxMap.get(flow)){maxMap.put(flow,value);} //Is this larger then previous value?
        	    avgMap.put(flow, avgMap.get(flow) + value); // Add to sum
        	    
        	}	
        	//save as csv
         	List<String[]> dataLines = new ArrayList<>();
        	dataLines.add(new String[] { "Flow id" });

    		
        	for (Entry<String, List<String>> entry : resultMap.entrySet()) {
        	    String flowID = entry.getKey();
        	    avgMap.put(flowID, avgMap.get(flowID) / entry.getValue().size()); //Dived by number of entries to get avgerage
        	    entry.getValue().add(0,flowID);
        	    String[] array = entry.getValue().toArray(new String[0]);
        	    dataLines.add(array);    
        	}
        	dataLines.add(new String[] { "" });
        	dataLines.add(new String[] { "Flow id","Min","Max","Avg" });
    	    for (String flowid : uniqueId) {
    	    	String[] array = new String[4];
    	    	array[0] = flowid;
    	    	array[1] = Double.toString(minMap.get(flowid));
    	    	array[2] = Double.toString(maxMap.get(flowid));
    	    	array[3] = Double.toString(avgMap.get(flowid));
    	    	dataLines.add(array); 
    	    	
			}
        	
        	givenDataArray_whenConvertToCSV_thenOutputCreated(dataLines);
        	
    		MessageDialog.openInformation(this.window.getShell(), "Info", "Generated CSV sucessfully");

    	    
    	} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
    	
    	
    	
    	
    	
    	
    	
    	


		
	}

    private String GetDiliminer(List<String> vectorIdList, List<String> vectorDelayList) {
		String delim = "^(";
		Boolean first = true;
		if(vectorIdList!=null){
			for (String string : vectorIdList) {
				if(!first){
					delim += "|";
					
				}
				delim += string;
			}
		}
		if(vectorDelayList!=null){
			for (String string : vectorDelayList) {
				if(!first){
					delim += "|";
					
				}
				delim += string;
			}
		}
		
		return delim + ")";
	}

	private void ReadUntilNew(String line, BufferedReader br) {
		// TODO Auto-generated method stub
		
	}

	public String convertToCSV(String[] data) {
        return Stream.of(data)
        	      .map(this::escapeSpecialCharacters)
        	      .collect(Collectors.joining(","));
    }
    public void givenDataArray_whenConvertToCSV_thenOutputCreated(List<String[]> dataLines) throws IOException {
        File csvOutputFile = new File( theFile.getParent().getLocation().toString() + "/" + theFile.getParent().getLocation().lastSegment().toString() + ".csv");
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
              .map(this::convertToCSV)
              .forEach(pw::println);
        }
        
    }
    
    
    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
    
    
    
    
    
    
    
	private boolean extractProjectAndFileFromInitiatingEvent(ExecutionEvent event) {
        // ============================================================================================================
        // The execute method of the handler is invoked to handle the event. As we only contribute to Explorer
        // Navigator views we expect to get a selection tree event
        // ============================================================================================================
        this.window = HandlerUtil.getActiveWorkbenchWindow(event);
        // Get the active WorkbenchPage
        this.activePage = this.window.getActivePage();

        // Get the Selection from the active WorkbenchPage page
        ISelection selection = this.activePage.getSelection();
        if(selection instanceof ITreeSelection) {
            TreeSelection treeSelection = (TreeSelection) selection;
            TreePath[] treePaths = treeSelection.getPaths();
            TreePath treePath = treePaths[0];

            // The TreePath contains a series of segments in our usage:
            // o The first segment is usually a project
            // o The last segment generally refers to the file

            // The first segment should be a IProject
            Object firstSegmentObj = treePath.getFirstSegment();
            this.theProject = (IProject) ((IAdaptable) firstSegmentObj).getAdapter(IProject.class);
            if(this.theProject == null) {
                MessageDialog.openError(this.window.getShell(), "Navigator Popup", getClassHierarchyAsMsg(
                                "Expected the first segment to be IAdapatable to an IProject.\nBut got the following class hierarchy instead.", "Make sure to directly select a file.",
                                firstSegmentObj));
                return false;
            }

            // The last segment should be an IResource
            Object lastSegmentObj = treePath.getLastSegment();
            this.theResource = (IResource) ((IAdaptable) lastSegmentObj).getAdapter(IResource.class);
            if(this.theResource == null) {
                MessageDialog.openError(this.window.getShell(), "Navigator Popup", getClassHierarchyAsMsg(
                                "Expected the last segment to be IAdapatable to an IResource.\nBut got the following class hierarchy instead.", "Make sure to directly select a file.",
                                firstSegmentObj));
                return false;
            }

            // As the last segment is an IResource we should be able to get an IFile reference from it
            this.theFile = (IFile) ((IAdaptable) lastSegmentObj).getAdapter(IFile.class);

            // Extract additional information from the IResource and IProject
            this.workspaceName = this.theResource.getWorkspace().getRoot().getLocation().toOSString();
            this.projectName = this.theProject.getName();
            this.fileName = this.theResource.getName();

            return true;
        } else {
            String selectionClass = selection.getClass().getSimpleName();
            MessageDialog.openError(this.window.getShell(), "Unexpected Selection Class", String.format("Expected a TreeSelection but got a %s instead.\nProcessing Terminated.", selectionClass));
        }

        return false;
    }

    @SuppressWarnings("rawtypes")
    private static String getClassHierarchyAsMsg(String msgHeader, String msgTrailer, Object theObj) {
        String msg = msgHeader + "\n\n";

        Class theClass = theObj.getClass();
        while(theClass != null) {
            msg = msg + String.format("Class=%s\n", theClass.getName());
            Class[] interfaces = theClass.getInterfaces();
            for(Class theInterface : interfaces) {
                msg = msg + String.format("    Interface=%s\n", theInterface.getName());
            }
            theClass = theClass.getSuperclass();
        }

        msg = msg + "\n" + msgTrailer;

        return msg;
    }
}
