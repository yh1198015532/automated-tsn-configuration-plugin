package org.plugin.tsnsched.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.plugin.model.SwitchPort;
import org.plugin.model.TSNNodeType;
import org.plugin.model.TSNSwitch;
import org.plugin.model.XMLNode;


public class NodeInformationPopup extends Dialog {

	String nodeName = "Node";
	ArrayList<XMLNode> connections = new ArrayList<XMLNode>();
	SetupView setupView;
	private Text textAddress;
	private Text textPacketLength;
	private Text textSendInterval;
	private Text txtNonScheduledHost;
	private Text txtScheduledHost;
	private Text text_3;
	private Text txtSwitch;
	private Text textProcessingDelay;
	private Composite nodeSpecific;
	private StackLayout nodeSpecificLayout;
	private Table table;
	private Composite compositePort;
	/**
	 * Create the dialog.
	 * @param parentShell
	 */
	public NodeInformationPopup(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gl_container = new GridLayout(1, false);
		gl_container.marginLeft = 10;
		container.setLayout(gl_container);
		
		Label lblNodeName = new Label(container, SWT.NONE);
		lblNodeName.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.BOLD));
		lblNodeName.setText(nodeName);
		
		Composite composite = new Composite(container, SWT.NONE);
		GridLayout gl_composite = new GridLayout(2, false);
		gl_composite.marginWidth = 0;
		composite.setLayout(gl_composite);
		
		Label lblConnectedTo = new Label(composite, SWT.NONE);
		lblConnectedTo.setText("Connected to :");
		
		Composite compositePath = new Composite(composite, SWT.NONE);
		compositePath.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridData gd_compositePath = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_compositePath.heightHint = 20;
		gd_compositePath.widthHint = 325;
		compositePath.setLayoutData(gd_compositePath);
		compositePath.setBounds(0, 0, 64, 64);
		
		// Add a button for each connected node
		for (XMLNode xmlNode : connections) {
			Button btnNode = new Button(compositePath, SWT.NONE);
			
			btnNode.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					
					setupView.openInformationDialog(btnNode.getText(), NodeInformationPopup.this);
					
				}
			});
			btnNode.setText(xmlNode.toString());
			btnNode.setSize(btnNode.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		}

		
		// Controls which information to show
		nodeSpecific = new Composite(container, SWT.NONE);
		GridData gd_nodeSpecific = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_nodeSpecific.heightHint = 203;
		nodeSpecific.setLayoutData(gd_nodeSpecific);
		nodeSpecificLayout = new StackLayout();
		nodeSpecific.setLayout(nodeSpecificLayout);
		
		// Node - compVlanEtherHostFullLoad
		Composite compVlanEtherHostFullLoad = new Composite(nodeSpecific, SWT.NONE);
		GridLayout gl_compVlanEtherHostFullLoad = new GridLayout(2, false);
		gl_compVlanEtherHostFullLoad.marginWidth = 0;
		compVlanEtherHostFullLoad.setLayout(gl_compVlanEtherHostFullLoad);
		
		Label lblType = new Label(compVlanEtherHostFullLoad, SWT.NONE);
		lblType.setText("Type");
		
		txtNonScheduledHost = new Text(compVlanEtherHostFullLoad, SWT.BORDER);
		txtNonScheduledHost.setText("Non scheduled host");
		txtNonScheduledHost.setEditable(false);
		txtNonScheduledHost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblSendInterval = new Label(compVlanEtherHostFullLoad, SWT.NONE);
		lblSendInterval.setBounds(0, 0, 55, 15);
		lblSendInterval.setText("Send Interval");
		
		textSendInterval = new Text(compVlanEtherHostFullLoad, SWT.BORDER);
		textSendInterval.setText("1ms");
		textSendInterval.setEditable(false);
		textSendInterval.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblPacketLength = new Label(compVlanEtherHostFullLoad, SWT.NONE);
		lblPacketLength.setBounds(0, 0, 55, 15);
		lblPacketLength.setText("Packet Length");
		
		textPacketLength = new Text(compVlanEtherHostFullLoad, SWT.BORDER);
		textPacketLength.setEnabled(true);
		textPacketLength.setEditable(false);
		textPacketLength.setText("1500 Byte");
		textPacketLength.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblAddress = new Label(compVlanEtherHostFullLoad, SWT.NONE);
		lblAddress.setBounds(0, 0, 55, 15);
		lblAddress.setText("Address");
		
		textAddress = new Text(compVlanEtherHostFullLoad, SWT.BORDER);
		textAddress.setEditable(false);
		textAddress.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		// Node - compVlanEtherHostSched
		Composite compVlanEtherHostSched = new Composite(nodeSpecific, SWT.NONE);
		GridLayout gl_compVlanEtherHostSched = new GridLayout(2, false);
		gl_compVlanEtherHostSched.marginWidth = 0;
		compVlanEtherHostSched.setLayout(gl_compVlanEtherHostSched);
		
		Label lblType_1 = new Label(compVlanEtherHostSched, SWT.NONE);
		lblType_1.setText("Type");
		
		txtScheduledHost = new Text(compVlanEtherHostSched, SWT.BORDER);
		txtScheduledHost.setText("Scheduled host");
		txtScheduledHost.setEditable(false);
		txtScheduledHost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblAddress_1 = new Label(compVlanEtherHostSched, SWT.NONE);
		lblAddress_1.setText("Address");
		
		text_3 = new Text(compVlanEtherHostSched, SWT.BORDER);
		text_3.setEditable(false);
		text_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Composite compVlanEtherSwitchPreemptable = new Composite(nodeSpecific, SWT.NONE);
		GridLayout gl_compVlanEtherSwitchPreemptable = new GridLayout(2, false);
		gl_compVlanEtherSwitchPreemptable.marginWidth = 0;
		compVlanEtherSwitchPreemptable.setLayout(gl_compVlanEtherSwitchPreemptable);
		
		Label lblType_1_1 = new Label(compVlanEtherSwitchPreemptable, SWT.NONE);
		lblType_1_1.setText("Type");
		
		txtSwitch = new Text(compVlanEtherSwitchPreemptable, SWT.BORDER);
		txtSwitch.setText("Switch");
		txtSwitch.setEditable(false);
		txtSwitch.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblProcessingDelay = new Label(compVlanEtherSwitchPreemptable, SWT.NONE);
		lblProcessingDelay.setText("Processing \nDelay");
		
		textProcessingDelay = new Text(compVlanEtherSwitchPreemptable, SWT.BORDER);
		textProcessingDelay.setText("5 us"); // TODO import right data
		textProcessingDelay.setEditable(false);
		textProcessingDelay.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(compVlanEtherSwitchPreemptable, SWT.NONE);
		new Label(compVlanEtherSwitchPreemptable, SWT.NONE);
		
		Label lblConnectedPorts = new Label(compVlanEtherSwitchPreemptable, SWT.NONE);
		lblConnectedPorts.setText("Ports :");
		
		compositePort = new Composite(compVlanEtherSwitchPreemptable, SWT.NONE);
		compositePort.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridData gd_compositePort = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_compositePort.heightHint = 20;
		gd_compositePort.widthHint = 325;
		compositePort.setLayoutData(gd_compositePort);
		compositePort.setBounds(0, 0, 64, 64);
		
		TSNSwitch switchInfo = this.setupView.switchMap.get(nodeName); // Get switch information (returns null if not switch)
		if(switchInfo!=null) {
			// Add a button for each port
			for (Integer temp : switchInfo.getports()) {
				Button btnNode = new Button(compositePort, SWT.NONE);
				
				btnNode.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						SwitchPort result = OpenPortDialog(btnNode.getText(),switchInfo.getPort(temp));
						if( result!=null ) {
							switchInfo.setPort(temp,result);
						}
					}
	
				});
				btnNode.setText(temp.toString());
				btnNode.setSize(btnNode.computeSize(SWT.DEFAULT, SWT.DEFAULT));
			}
		}

		
		
		

		

		
		setInformation(setupView.nodeToTypeMap.get(nodeName));
		
		
		// Resize the shell to fit the buttons
		/*
		Shell shell = this.getParentShell();
			
		shell.layout(true, true);

		final Point newSize = shell.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);  

		shell.setSize(newSize);
		 */
		return container;
	}


	private SwitchPort OpenPortDialog(String text, SwitchPort switchPort) {
		
		Shell shell = compositePort.getShell();
		PortDialog dialog = new PortDialog(shell,setupView,nodeName);
		dialog.setSwitchPort(switchPort);
		if (dialog.open() == Window.OK) {
			// Save all the port entries in a new list
			try {
				switchPort = (SwitchPort) dialog.switchPort.clone();
				return switchPort;
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
		
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 350);
	}
	
	public void setNodeName(String string) {
		nodeName = string;
	}

	public void setConnections(ArrayList<XMLNode> children) {
		connections = children;
	}

	public void setCallback(SetupView setupView) {
		this.setupView = setupView;
		
	}
	public void setInformation(TSNNodeType type) {
		switch(type) {
		case NON_SCHED_HOST:
			textAddress.setText(setupView.nodeToAddressMap.get(nodeName));
			nodeSpecificLayout.topControl = nodeSpecific.getChildren()[0];
			nodeSpecific.layout();
			break;
		case SCHED_HOST:
			text_3.setText(setupView.nodeToAddressMap.get(nodeName));
			nodeSpecificLayout.topControl = nodeSpecific.getChildren()[1];
			nodeSpecific.layout();
			break;
		case SWITCH:
			nodeSpecificLayout.topControl = nodeSpecific.getChildren()[2];
			nodeSpecific.layout();
			break;
		default:
			break;
		
		}
	}
}
