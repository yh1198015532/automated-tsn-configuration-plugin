package org.plugin.toolbar;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.plugin.tsnsched.views.SetupView;


public class ToolbarAction implements IWorkbenchWindowActionDelegate {
	IWorkbenchWindow activeWindow = null;
    private IWorkbenchWindow window;
    private IWorkbenchPage activePage;

    private IProject theProject;
    private IResource theResource;
    private IFile theFile;

    private String workspaceName;
    private String projectName;
    private String fileName;
    private String path;
	/** Run the action. Display the Hello World message
	 */		
	public void run(IAction proxyAction) {
		
        // React to the popup selected
    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		Shell shell = activeWindow.getShell();

        try {
        	// Read the file after selection
        	SetupView viewPart = (SetupView)page.showView("org.plugin.tsnsched.views.SetupView");
        	
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}

	// IActionDelegate method
	public void selectionChanged(IAction proxyAction, ISelection selection) {
		// do nothing, action is not dependent on the selection
	}
	
	// IWorkbenchWindowActionDelegate method
	public void init(IWorkbenchWindow window) {
		activeWindow = window;
	}
	
	// IWorkbenchWindowActionDelegate method
	public void dispose() {
		//  nothing to do
	}
	   private boolean extractProjectAndFileFromInitiatingEvent(ExecutionEvent event) {
	        // ============================================================================================================
	        // The execute method of the handler is invoked to handle the event. As we only contribute to Explorer
	        // Navigator views we expect to get a selection tree event
	        // ============================================================================================================
	        this.window = HandlerUtil.getActiveWorkbenchWindow(event);
	        // Get the active WorkbenchPage
	        this.activePage = this.window.getActivePage();

	        // Get the Selection from the active WorkbenchPage page
	        ISelection selection = this.activePage.getSelection();
	        if(selection instanceof ITreeSelection) {
	            TreeSelection treeSelection = (TreeSelection) selection;
	            TreePath[] treePaths = treeSelection.getPaths();
	            TreePath treePath = treePaths[0];

	            // The TreePath contains a series of segments in our usage:
	            // o The first segment is usually a project
	            // o The last segment generally refers to the file

	            // The first segment should be a IProject
	            Object firstSegmentObj = treePath.getFirstSegment();
	            this.theProject = (IProject) ((IAdaptable) firstSegmentObj).getAdapter(IProject.class);
	            if(this.theProject == null) {
	                MessageDialog.openError(this.window.getShell(), "Navigator Popup", getClassHierarchyAsMsg(
	                                "Expected the first segment to be IAdapatable to an IProject.\nBut got the following class hierarchy instead.", "Make sure to directly select a file.",
	                                firstSegmentObj));
	                return false;
	            }

	            // The last segment should be an IResource
	            Object lastSegmentObj = treePath.getLastSegment();
	            this.theResource = (IResource) ((IAdaptable) lastSegmentObj).getAdapter(IResource.class);
	            if(this.theResource == null) {
	                MessageDialog.openError(this.window.getShell(), "Navigator Popup", getClassHierarchyAsMsg(
	                                "Expected the last segment to be IAdapatable to an IResource.\nBut got the following class hierarchy instead.", "Make sure to directly select a file.",
	                                firstSegmentObj));
	                return false;
	            }

	            // As the last segment is an IResource we should be able to get an IFile reference from it
	            this.theFile = (IFile) ((IAdaptable) lastSegmentObj).getAdapter(IFile.class);

	            // Extract additional information from the IResource and IProject
	            this.workspaceName = this.theResource.getWorkspace().getRoot().getLocation().toOSString();
	            this.projectName = this.theProject.getName();
	            this.fileName = this.theResource.getName();

	            return true;
	        } else {
	            String selectionClass = selection.getClass().getSimpleName();
	            MessageDialog.openError(this.window.getShell(), "Unexpected Selection Class", String.format("Expected a TreeSelection but got a %s instead.\nProcessing Terminated.", selectionClass));
	        }

	        return false;
	    }

	    @SuppressWarnings("rawtypes")
	    private static String getClassHierarchyAsMsg(String msgHeader, String msgTrailer, Object theObj) {
	        String msg = msgHeader + "\n\n";

	        Class theClass = theObj.getClass();
	        while(theClass != null) {
	            msg = msg + String.format("Class=%s\n", theClass.getName());
	            Class[] interfaces = theClass.getInterfaces();
	            for(Class theInterface : interfaces) {
	                msg = msg + String.format("    Interface=%s\n", theInterface.getName());
	            }
	            theClass = theClass.getSuperclass();
	        }

	        msg = msg + "\n" + msgTrailer;

	        return msg;
	    }
}